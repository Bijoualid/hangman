package co.simplon.promo16;

import co.simplon.promo16.hangmen.Hangman;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Hangman potence = new Hangman();
        potence.start();
    }
}
