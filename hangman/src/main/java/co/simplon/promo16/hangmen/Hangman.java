package co.simplon.promo16.hangmen;

import java.util.Scanner;

public class Hangman {
    private static Scanner scanner = new Scanner(System.in);
    private static String word;
    private static String hiddenWord;
    private static boolean winner = false;

    /**
     * Méthode principale pour lancer le jeu.
     */
    public void start() {

        System.out.println("Entrez un mot à devinez: ");
        // Enregistrement du mot entré par l'utilisateur.
        word = scanner.nextLine().toLowerCase();

        hiddenWord = wordToQuestionMarks(word);
        System.out.println("Le mot à deviner est enregistrer");

        //Le nombre de tentatives est égale aux nombres de charactères du mot à deviner.
        int amountofGuesses = word.length();
        int tries = 0;

        //Boucle tant qu'il n'y a pas de winner ou que toutes les tentatives ont été utilisées.
        while (!winner && tries != amountofGuesses) {
            guessLetter();
            tries++;
        }

        if (!winner) {
            System.out.println("Vous avez épuisé vos tentatives");
            System.out.println("le mot à deviner était: " + word);

        } else {

            System.out.println("Bravo! Vous avez gagné!");
        }
    }

/**
 * Conversion du mot enregistré en (?).
 * @param word Mot à deviner entré par l'utilisateur.
 * @return
 */
    public String wordToQuestionMarks(String word) {
        return word.replaceAll(".", "?");
    }

/**
 * Prise en compte de l'entrée de caractères par l'utilisateur.
 */
    public void guessLetter() {
        System.out.println("Mot à deviner: " + hiddenWord);

        System.out.println("Deviné une lettre");
        String letterChoice = scanner.nextLine().toLowerCase();

        int found = 0;

        if (hasLetter(letterChoice)) {
            found = updateGameState(letterChoice);
        }

        System.out.println("Vous avez trouvé " + found + " " + letterChoice + "\n");
        gameOver();
    }

    /**
     * Mise à jour de l'état du jeu.
     * @param letter
     * @return
     */
    private static int updateGameState(String letter) {
        int found = 0;

        for(int i=0; i < word.length(); i++) {
            if (word.charAt(i) == letter.charAt(0)) {
                String prev = hiddenWord.substring(0, i).concat(letter);
                hiddenWord = prev.concat(hiddenWord.substring(i+1));
                found++;
            }
        } return found;
    } 

    public void gameOver() {
        if (!hiddenWord.contains("?")) {
            winner = true;
        }
    }

    public boolean hasLetter(String letter) {
        if (word.contains(letter)) {
            return true;
        }
        else {
            return false;
        }
    }
}
