# Explication du jeu

Un mot doit être deviné par le joueur.
Ainsi, l'écran de sortie affichera le nombre de ? représentant les lettres restant à deviner.

Ensuite, le joueur devra deviner une lettre.Si cette lettre est présente dans le mot, le programme remplacera les ? par la lettre à chaque endroit où elle apparaît.

Le joueur gagne la partie dès que toutes les lettres du mot ont été devinées correctement et perds la partie si ses tentatives sont épuisées.